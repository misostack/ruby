# Login to remote

```
ssh ubuntu@cloud002.local
12345678
```

# How to run

```
ruby example.rb
```

or

```
bundler exec ruby example.rb
```

or

```
cd basic
./filename.rb
```
