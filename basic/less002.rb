#!/usr/bin/env ruby
$angle_types = ['Hình vuông', 'Tam giác vuông cân', 'Tam giác cân']
$angle_types.each_with_index do |angle_type, index|
  puts "#{index+1}.#{angle_type}"
end
print "Vui lòng chọn hình : "
$angle_type = gets.chomp.to_i
print "Vui lòng nhập số tầng của hình #{$angle_types[$angle_type-1]}: "
$h = gets.chomp.to_i
$types = ['rỗng', 'đặc']
puts "1.Hình #{$angle_types[$angle_type-1]} rỗng"
puts "2.Hình #{$angle_types[$angle_type-1]} đặc"
print "Hình đặc hay rỗng(bấm 1 hoặc 2 để chọn) : "
$type = gets.chomp.to_i

puts "Đây là hình #{$angle_types[$angle_type-1]} #{$types[$type-1]} #{$h} tầng"
def draw_square(h, empty = false)
  $i = 1
  while $i <= h**2 do
    print '* '
    print "\n" if $i % h == 0
    $i += 1
  end
end

def draw_tam_giac_vuong_can(h)
  $x = 1
  while $x <= h do
    $y = 1
    while $y <= h do
      print '* ' if $x >= $y
      print ' ' if $x < $y
      $y += 1
    end
    print "\n"
    $x += 1
  end
end

def draw_tam_giac_can(h)
  $row = 1
  while $row <= h do
    $col = 1
    while $col <= 2 * (h-1) + 1 do
      print '* '
      $col += 1
    end
    print "\n"
    $row += 1
  end
end

# draw_square($h)
# puts ' '
# draw_tam_giac_vuong_can($h)
# puts ' '
draw_tam_giac_can($h)

# puts "1.Hình vuông"
# puts "1.Hình vuông"
# puts "1.Hình vuông"
# puts "1.Hình vuông"
# puts "Vui lòng nhập số tầng của hình vuông đặc : "
# $h = gets.chomp.to_i
# puts "Đây là hình vuông đặc #{$h} tầng"
# $row = 1
# while $row <= $h*$h do
#   print "*"
#   print "\n" if($row % $h == 0)
#   $row += 1
# end
