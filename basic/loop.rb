#!/usr/bin/env ruby

puts 'Example for ruby loops'
# while
i = 0
while i < 10
  i += 1
  puts i.to_s
end
puts defined?(i) ? "i=#{i}" : 'i=nil'
# for
puts '.' * 10
for j in 1..10
  puts j.to_s
end
puts defined?(j) ? "j=#{j}" : 'j=nil'
# each
puts '.' * 10
(1..10).each do |x|
  puts x.to_s
end
puts defined?(x) ? "x=#{x}" : 'x=nil'
