#!/usr/bin/env ruby

puts 'Example for ifelse'

puts '1 < 2' if 1 < 2
def la_hinh_vuong?(chieu_dai, chieu_rong)
  chieu_dai == chieu_rong
end

def kiem_tra_nam_nhuan?(nam)
  nam_nhuan = false
  if nam % 4 == 0 && nam % 100 !=0
    nam_nhuan = true
  elsif nam % 400 == 0
    nam_nhuan = true
  else
    nam_nhuan = false
  end
  nam_nhuan
end

puts 'Hinh chu nhat co chieu_dai=2 va chieu_rong=2 la hinh vuong' if la_hinh_vuong?(2,2)
puts 'Nam 2012 la nam nhuan' if kiem_tra_nam_nhuan?(2012)
puts 'Nam 1000 khong phai la nam nhuan' unless kiem_tra_nam_nhuan?(1000)

# Case

def capacity_check(capacity)
  case capacity
  when 0
    "You ran out of gas."
  when 1..20
    "The tank is almost empty. Quickly, find a gas station!"
  when 21..70
    "You should be ok for now."
  when 71..100
    "The tank is almost full."
  else
    "Error: capacity has an invalid value (#{capacity})"
  end
end

puts capacity_check(0)

def serial_check(serial)
  case serial
  when /\AC/
    "Low risk"
  when /\AL/
    "Medium risk"
  when /\AX/
    "High risk"
  else
    "Unknown risk"
  end
end
puts serial_check('C')
puts serial_check('L')
puts serial_check('X')
puts serial_check('Z')

SITES = {
  "europe"  => "http://eu.example.com",
  "america" => "http://us.example.com"
}

country = 'europe'

puts SITES[country]
