#!/usr/bin/env ruby

puts "Đề bài : Hiển thị bảng cửu chương tương ứng với số của người dùng nhập vào, giới hạn từ 1-10"

number = ARGV[0].to_i

puts "Sau đây là bảng cửu chương #{number}"

puts "#{1+2}"

(1..10).each do |i|
  puts "#{number} x #{i} = #{number * i }"
end

puts "." * 10  # what'd that do?

formatter = '%{first} %{second} %{third} %{fourth}'

puts formatter % {first: 1, second: 2, third: 3, fourth: 4}
