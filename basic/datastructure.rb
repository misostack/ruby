#!/usr/bin/env ruby

puts 'Example for data structure'
# array
animals = ['cat', 'dog', 'bird', 'cow', 'chicken']

animals.each { |e| puts e }

animals.each_with_index { |e, index|
  puts "#{index + 1}.#{e}"
}

weekdays = %w[
  Sunday Monday Tuesday Wednesday Thursday Friday Saturday
]

(0..6).each { |i| puts weekdays[i] }

students = []
students.push('Lan')
students << 'Huy'
students << 'Phuong'

puts "There #{students.length} students:"
students.each { |e| puts e }


# hash,keys
people = {
  'name' => 'Son',
  'nickname' => 'miso',
  'age' => 30,
  'nationality' => 'Vietnamese'
}

puts "My name is #{people['name']}"
puts "My nickname is #{people['nickname']}"
puts "I'm #{people['age']} years old"
puts "I'm #{people['nationality']}"

# Using symbols as Hash Keys
test_result = Hash.new
test_result[:name] = 'miso'
test_result[:math] = 10
test_result[:physic] = 8
test_result[:math] = 9 # final math points

puts "The test results of #{test_result[:name]} is:"
puts "1.Math : #{test_result[:math]}"
puts "2.Physic : #{test_result[:physic]}"

# loop
test_result.each do |attribute, value|
  puts "#{attribute}: #{value}"
end
